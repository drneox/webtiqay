from wtforms import Form, BooleanField, TextField, PasswordField, validators


class ClientForm(Form):
    name = TextField('Name', [validators.Length(min=4, max=25)])
    email = TextField('Email', [validators.Length(min=6, max=35)])
    url = TextField('URL', [validators.Length(min=6, max=35)])