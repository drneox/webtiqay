from sys import path
from config import WATIQAY_PATH

path.append(WATIQAY_PATH)
from flask import Flask, render_template, request, redirect, url_for
from mongoengine import connect
from db import Log, Client
from forms import ClientForm

app = Flask(__name__)

connect("watiqay")


@app.route("/")
def hello(name=None):
    return render_template("index.html", name=name)


@app.route("/log/<id>")
@app.route("/log")
def log(id=None):
    if id:
        client = Client.objects.get(uid=id)
        log = Log.objects(client=client)
    else:
        log = Log.objects()
    return render_template("log.html", logs=log)

@app.route("/client", methods=['GET', 'POST'])
def client():
    form = ClientForm(request.form)
    if request.method == 'POST' and form.validate():
        id = Client.objects.count() + 1
        print id
        user = Client(uid=Client.objects.count() + 1, name=form.name.data, email=form.email.data, url=form.url.data)
        user.save()
        return redirect("/client")
    client = Client.objects()
    return render_template("client.html", clients=client, form=form)

if __name__ == "__main__":
    app.run(debug=True, host='localhost')